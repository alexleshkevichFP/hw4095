const setConfig = (elem) => {
    if (!elem) {
        return;
    }

    const configInfo = elem.dataset;

    console.log(configInfo)


    document.querySelectorAll(`#formMethod option`).forEach((item) => {
        item.removeAttribute('selected')
        if (item.value === configInfo.formMethod) {
            item.setAttribute('selected', true);
        }
    });
    document.querySelectorAll(`#formDataType option`).forEach((item) => {
        item.removeAttribute('selected')
        if (item.value === configInfo.type) {
            item.setAttribute('selected', true);
        }
    });
    document.querySelector('#formUrl').value = configInfo.extUrl;
    document.querySelector('#reqBody').value = configInfo.reqBody;
    document.querySelector('#formParams').value = configInfo.params;
}
