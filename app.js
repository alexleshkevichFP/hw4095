const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const fetch = require("isomorphic-fetch");

const {body, check, validationResult} = require('express-validator');

const TestConfig = require('./models/test-config');

const app = express();

app.set('view engine', 'ejs');
app.set('views', 'views');

let pageResult = null;

// body parser middleware
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
// share static files
app.use(express.static(path.join(__dirname, 'public')));

app.post('/run', [], async (req, res, next) => {

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.render('error-page', {pageTitle: 'Error', errors: JSON.stringify(errors.array())});
    }

    const body = req.body;

    let url = body.externalUrl;

    let fetchOptions = {
        method: body.formMethod
    }

    if (body.formMethod === 'GET') {
        if (body.formParams) {
            url += body.formParams;
        }
    } else if (body.formMethod === 'POST') {

        let reqBody = '';
        try {
            reqBody = JSON.parse(body.formReqBody);

            if (body.formDataType === 'json') {
                reqBody = JSON.stringify(reqBody);
            } else {
                const esc = encodeURIComponent;
                const query = Object.keys(reqBody)
                    .map(k => esc(k) + '=' + esc(reqBody[k]))
                    .join('&');

                fetchOptions = {
                    method: body.formMethod,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
                    },
                    body: query,
                }
            }
        } catch (err) {
            reqBody = '';

            fetchOptions = {
                method: body.formMethod,
                body: reqBody,
            }
        }
    }

    console.log(url, fetchOptions);
    // console.log(body.formMethod);

    fetch(url, fetchOptions).then((response) => {
        console.log(req.body)
        pageResult = {
            status: response.status,
            statusText: response.statusText,
            headers: [],
            url: response.url,
        };

        for (let header of response.headers) {
            pageResult.headers.push(
                {
                    name: header[0],
                    value: header[1]
                }
            );
        }




        return response.text();
    }).then((response) => {
        pageResult.content = response;
        if (req.body.saveConfig) {
            const confId = Math.random();
            const confExternalUrl = req.body.externalUrl;
            const confFormMethod = req.body.formMethod;
            const confFormDataType = req.body.formDataType;
            const confFormReqBody = req.body.formReqBody;
            const confFormParams = req.body.formParams;

            const testConfig = new TestConfig(
                confId,
                confExternalUrl,
                confFormMethod,
                confFormDataType,
                confFormReqBody,
                confFormParams
            )

            testConfig.save();
        }
        res.redirect('/result-page');
    });
});

app.get('/result-page', (req, res) => {
    try {
        res.render('result-page', {
            pageTitle: 'result',
            content: JSON.stringify(pageResult.content),
            status: pageResult.status,
            statusText: pageResult.statusText,
            headers: pageResult.headers,
            url: pageResult.url
        });
    } catch (err) {
        res.render('result-page', {
            pageTitle: 'result',
            content: null,
            status: null,
            statusText: null,
            headers: null,
            url: null
        });
    }
});

app.get('/', [], (req, res) => {
    const savedConfigs = TestConfig.getAllConfigs(configs => {
        res.render('form-page', {pageTitle: 'Form page', configList: configs});
    })

});

app.use((req, res) => {
    res.render('404', {pageTitle: 'Not found'});
});

app.listen(7480);
