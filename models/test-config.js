const fs = require('fs');
const path = require('path');

const p = path.join(path.dirname(require.main.filename), 'data', 'test-configs.json')

const getConfigsFromFile = (cb) => {
    fs.readFile(p, (err, fileContent) => {
        if (err) {
            cb([]);
        } else {
            cb(JSON.parse(fileContent))
        }
    })
}

module.exports = class Product {

    constructor(id, externalUrl, formMethod, formDataType, formReqBody, formParams) {
        this.id = id;
        this.externalUrl = externalUrl;
        this.formMethod = formMethod;
        this.formDataType = formDataType;
        this.formReqBody = formReqBody;
        this.formParams = formParams;
    }

    save() {
        getConfigsFromFile(configs => {
            configs.push(this);
            fs.writeFile(p, JSON.stringify(configs), (err) => {
                console.log('ERR: ', err);
            });
        });
    }

    static getAllConfigs(cb) {
        getConfigsFromFile(cb);
    }
}
